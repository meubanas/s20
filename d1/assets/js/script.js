/* arrow function */

// function sampleFunction(){
// 	alert('Hellow, I am filipino');
// }
// sampleFunction();

// // convert to ES6
// const sampleFunction1 = () => {
// 	alert('I am an arrow  function')
// };
// sampleFunction1();

// /* implicit return with arrow function
//  tradition function */
// function prodNum(num1, num2){
// 	return num1 * num2
// };

// let prod = prodNum(25, 25);
// console.log(prod); 

// // convert to arrow function
// const prodNum1 = (num1, num2) => num1 * num2;
// let product = prodNum1(25, 10);
// console.log(product);

// const multiplyNum = (num1, num2) => {
// 	return num1*num2;
// };

// let prod2 = multiplyNum(5, 10);
// console.log(prod2);

// // traditional function
// function ans(a){
// 	if(a < 10){
// 		return 'true';
// 	} else {
// 		return 'false';
// 	}
// };

// let answer = ans(5);
// console.log(answer);

// convert to arrow function
// ternary operator ?: - syntax: condition ? expression1 ;
const ans1 = (a1) => (a1 < 10) ? 'true' : 'false';
let answer1 = ans1(5);
console.log(answer1);

// another example for ternary operator
let mark = prompt('Enter your grade');
let grade = (mark >= 75) ? 'Pass' : 'Fail'
console.log(`You ${grade} the exam.`);

/*
  convert the following function into an arrow function and display the result in console.

	let numbers = [1, 2, 3, 4, 5];
	
	let allValid = numbers.every(function(number){
		return (number < 3)
	});

	let filterValid = numbers.filter(function(number){
		return (number < 3)
	})
	
	let numberMap = numbers.map(function(number){
		return number * number
	})

*/
let number = [1, 2, 3, 4, 5];

const allValid = number.every(number => number <3);
const filterValid = number.filter(number => number <3);
const numberMap = number.map(number => number * 3);

console.log(allValid);
console.log(filterValid);
console.log(numberMap);

// ternary operator example with three expression
// to check if number is positive, negative or zero
let num = 3;
let result = (num >= 0) ? (num == 0 ? 'zero' : 'positive') : 'negative';
console.log(`The number is ${result}`);

// JSON - Javascript Object Notation
	// JSON is a string but formattedas JS Object
	// JSON  is popularly used to pass data from one application to another
	// JSON is not only in JS but also in other programming languanges to pass data
	// this is why it is specified as Javascript Object Notation
	// file extension .json
	// There is a way to turn JSON as JS Objects and there is way turn JS Objects to JSON.

// JS Object are not the same as JSON
	// JSON is a string
	// JS Object is an object
	// JSON keys are surrounded with double quotes.

	// Syntax for JSON
	/*
		{
			"key1":"value",
			"key2":"value",
		}
	supoort JSON
	- string
	- number
	- object
	- array
	- boolean
	-n ull
	*/
// example JSON
const person = {
	"name" : "Juan",
	"weight" : 175,
	"age" : 20,
	"eyeColor": "brown",
	"cars" : ["Toyota", "Honda"],
	"favoriteBooks" : {
		"title" :  "When the  fire Nation Attack",
		"author" : "Nickeloden",
		"release" : "2021"
	}
};

console.log(person);

/*Mini-Activity
	Create an array of JSON format data with atleast 2 items
	Name the array as assets
	Each JSON format data should have the following key-value pairs:

		id - <value>
		name - <value>
		description - <value>
		isAvailable - <value>
		dateAdded - <value> 
*/

const assets =[
{
	"id" : "bag",
	"name" : "LV",
	"description" : "leather",
	"isAvailable" : true,
	"dateAdded" : "April 2021", 
},

{
	"id" : "shoes",
	"name" : "jordan air",
	"description" : "sports",
	"isAvailable" : false,
	"dateAdded" : "December 2020"
}

];
console.log(assets);

// XML format- extensible  markup language
/*
	<note>
		<to>Juan</to>
		<from>Maria Clara</from>
		<heading>Reminder</heading>
		<body>Don't forget me this weekend</body>
	</note>

*/
//convert from XML to JSON - key value pairs -string
// {
// 	"to" : "juan",
// 	"from" : "Maria Clara",
// 	"heading" : "Reminder",
// 	"body" : "don't forget me this weekend!"
// }

// Stringify - method to convert Javascript Object to JSON and  vice versa.
// Converting JS Objects into JSON
	// this is  commonly used  whentrying to pass data from one application to another  via the use  HTTP Request.
	// HTTP requests are  request from resource between server and a client(browser) 
	// JSON format is  also  in database

// Object Javascript
const cat = {
	"name" : "Mashiro",
	"age" : 3,
	"weigh" : 20
};

console.log(cat);
// convert to JSON string
const catJSON = JSON.stringify(cat);
console.log(catJSON);

// parse - reverse of using strinify
const  json = '{"name":"Mashiro","age":3,"weigh":20}';
const  cat1 = JSON.parse(json);
console.log(cat1);

// from string convert to object
let batchesArr = [
	{
		batchName : "Batch 131"
	},
	{
		batchName : "Batch 132"
	}
];
// use by JSON.stringify
console.log(JSON.stringify(batchesArr));

// from object convert to string
let batchesJSON = `[
	{
		"batchName":"Batch 131"
	},
	{
		"batchName":"Batch 132"
	}
]`;

console.log(JSON.parse(batchesJSON));

/*
	Create an arrow function called postCourse which allows us to add a new object into the array. It should receive data: id, name, description, price, isActive.

	Add the new course into the array and show the following alert:
		"You have created <nameOfCourse>. The price is <priceOfCourse>."

	Create an arrow function which allows us to find a particular course providing the course id and return the details of the found course.
		- use find()

	Create an arrow function called deleteCourse which can delete the last course object in the array.
		- pop()
 */

// Pushing Instructions:

// 	Go to Gitlab:
// 		-in your zuitt-projects folder and access b131 folder.
// 		-inside your b131 folder create a new folder/subgroup: s20
// 		-inside s20, create a new project/repo called activity
// 		-untick the readme option
// 		-copy the git url from the clone button of your activity repo.

// 	Go to Gitbash:
// 		-go to your b131/s20 folder and access activity folder
// 		-initialize activity folder as a local repo: git init
// 		-connect your local repo to our online repo: git remote add origin <gitURLOfOnlineRepo>
// 		-add your updates to be committed: git add .
// 		-commit your changes to be pushed: git commit -m "includes JSON activity 1"
// 		-push your updates to your online repo: git push origin master

// 	Go to Boodle:
// 		-copy the url of the home page for your s20/activity repo (URL on browser not the URL from clone button) and link it to boodle:

// 		WD078-20 | Javascript - Introduction to JSON



const postCourse




